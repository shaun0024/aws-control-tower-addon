"""Common modules"""
import logging
from os import environ

"""Modules required by this file"""
from schema import Schema, Optional, And, Regex


def __get_schema_attributes():
    """Returns attributes required by the Account and Contact schema.

    Parameters:
    ----------
    - None

    Return:
    ------
    dictionary
    """

    def __get_extended_account_attributes():
        """Returns attributes required by the Account schema.

        Parameters:
        ----------
        - None

        Return:
        ------
        dictionary
        """

        extended_account_attributes = {
            'OwningBusinessUnit': {
                'Mandatory': True,
                'Type': str,
                'Regex': r'^BU(\d){3,6}$'
            },
            'CostCode': {
                'Mandatory': True,
                'Type': str,
                'Regex': r'^COST(\d){3,6}$'
            },
            'Comments': {
                'Mandatory': False,
                'Type': str
            }
        }

        return extended_account_attributes


    def __get_contact_attributes():
        """Returns attributes required by the Contact schema.

        Parameters:
        ----------
        - None

        Return:
        ------
        dictionary
        """

        contact_attributes = {
            'ContactName': {
                'Mandatory': True,
                'Type': str
            },
            'ContactTitle': {
                'Mandatory': False,
                'Type': str
            },
            'ContactEmail': {
                'Mandatory': True,
                'Type': str,
                'Regex': get_common_pattern('Email')
            },
            'ContactPhone': {
                'Mandatory': False,
                'Type': str
            }
        }

        return contact_attributes


    return __get_extended_account_attributes(), __get_contact_attributes()



def get_common_pattern(pattern_type):
    """Returns common regular expression patterns.

    Parameters:
    ----------
    - pattern_type [string]: The type of regular expression pattern.

    Return:
    ------
    string
    """

    regex_patterns = {
        'Email': r'^[\a-zA-Z0-9\-\._+]+@([\a-zA-Z0-9\-]+\.)+[\a-zA-Z0-9\-]{2,4}$',
        'AwsOrgUnitId': r'^ou-[0-9a-z]{4,32}-[a-z0-9]{8,32}$',
        'AwsAccountId': r'^\d{12}$'
    }

    return regex_patterns[pattern_type]



def validate_input_schema(action, input_data):
    """Validates the schema of the payload.

    Parameters:
    ----------
    - input_data [dictionary]: The contents of the payload.

    Return:
    ------
    dictionary
    """

    extended_account_attributes, contact_attributes = __get_schema_attributes()

    contact_schema = {}

    for attribute in contact_attributes:
        if contact_attributes[attribute]['Mandatory'] is True:
            contact_schema.update({ attribute: contact_attributes[attribute]['Type'] if 'Regex' not in contact_attributes[attribute] else And(str, Regex(contact_attributes[attribute]['Regex'])) })

        else:
            contact_schema.update({ Optional(attribute): contact_attributes[attribute]['Type'] if 'Regex' not in contact_attributes[attribute] else And(str, Regex(contact_attributes[attribute]['Regex'])) })


    account_schema = {
        'AwsAccountName': {
            'Required': [ 'PROVISION' ],
            'Optional': [ ],
            'Definition': str
        },
        'AwsAccountRootAddress': {
            'Required': [ 'PROVISION' ],
            'Optional': [ ],
            'Definition': And(str, Regex(get_common_pattern('Email')))
        },
        'AwsOrganisationalUnitId': {
            'Required': [ 'PROVISION', 'ENROL' ],
            'Optional': [ 'UPDATE' ],
            'Definition': And(str, Regex(get_common_pattern('AwsOrgUnitId')))
        },
        'AwsAccountSSOAddress': {
            'Required': [ 'PROVISION' ],
            'Optional': [ ],
            'Definition': And(str, Regex(get_common_pattern('Email')))
        },
        'AwsAccountSSOFirstName': {
            'Required': [ 'PROVISION' ],
            'Optional': [ ],
            'Definition': str
        },
        'AwsAccountSSOLastName': {
            'Required': [ 'PROVISION' ],
            'Optional': [ ],
            'Definition': str
        },
        'AccountContacts': {
            'Required': [ 'PROVISION', 'ENROL' ],
            'Optional': [ 'UPDATE' ],
            'Definition': And(lambda n: len(n), [ contact_schema ])
        },
        'TechnicalContacts': {
            'Required': [ 'PROVISION', 'ENROL' ],
            'Optional': [ 'UPDATE' ],
            'Definition': And(lambda n: len(n), [ contact_schema ])
        },
        'OperationalContacts': {
            'Required': [ 'PROVISION', 'ENROL' ],
            'Optional': [ 'UPDATE' ],
            'Definition': And(lambda n: len(n), [ contact_schema ])
        }
    }

    for attribute in extended_account_attributes:
        account_schema.update({
            attribute: {
                'Required': [ 'PROVISION', 'ENROL' ] if extended_account_attributes[attribute]['Mandatory'] is True else [ ],
                'Optional': [ 'UPDATE' ] if extended_account_attributes[attribute]['Mandatory'] is True else [ 'PROVISION', 'ENROL', 'UPDATE' ],
                'Definition': extended_account_attributes[attribute]['Type'] if 'Regex' not in extended_account_attributes[attribute] else And(str, Regex(extended_account_attributes[attribute]['Regex']))
            }
        })

    constructed_schema = {}
    for attribute in account_schema:
        if action in account_schema[attribute]['Required']:
            constructed_schema.update({ attribute: account_schema[attribute]['Definition'] })

        elif action in account_schema[attribute]['Optional']:
            constructed_schema.update({ Optional(attribute): account_schema[attribute]['Definition'] })

    account_schema = Schema(constructed_schema)

    return account_schema.validate(input_data)