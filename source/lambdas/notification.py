"""Common modules"""
import logging
from os import environ

"""Modules required by this file"""
from boto3 import client

"""Logging settings"""
logger = logging.getLogger()
log_level = 'INFO' if 'LOGGING_LEVEL' not in environ else environ['LOGGING_LEVEL'].upper()
level = logging.getLevelName(log_level)
logger.setLevel(level)

"""Global variables"""
from_address = environ['FROM_ADDRESS']
ses_identity_arn = environ['SES_IDENTITY_ARN']

"""Global clients"""
ses = client('sesv2')


def __get_template(action, state, account, additional_messages = None):
    """Returns a notification template that contains the subject and body of the e-mail.

    Parameters:
    ----------
    - action [string]: The action being taken. Accepted values are 'PROVISION_START', 'PROVISION_END', 'ENROL_START', 'ENROL_END',
        'UPDATE_START', 'UPDATE_END' and 'UNENROL_START'.
    - state [string]: The state of the process. Accepted values are 'SUCCEEDED' and 'FAILED'.
    - account [dictionary]: A dictionary containing key / value pairs corresponding to the details of the account.
    - additional_messages [string][optional]: Any additional messages that are needed to be sent.

    Return:
    ------
    dictionary
    """

    templates = {
        'PROVISION_START': {
            'SUCCEEDED': {
                'Subject': f"AWS account {account['AwsAccountName']} has gone into the {account['OperationalStatus'].lower()} state",
                'Body': f"AWS account {account['AwsAccountName']} has gone into the {account['OperationalStatus'].lower()} state"
            },
            'FAILED': {
                'Subject': f"Error {account['OperationalStatus'].lower()} AWS account {account['AwsAccountName']}",
                'Body': f"There was a problem {account['OperationalStatus'].lower()} the AWS account {account['AwsAccountName']}: {additional_messages}"
            }
        },
        'ENROL_START': {
            'SUCCEEDED': {
                'Subject': f"AWS account {account['AwsAccountName']} has gone into the {account['OperationalStatus'].lower()} state",
                'Body': f"AWS account {account['AwsAccountName']} has gone into the {account['OperationalStatus'].lower()} state"
            },
            'FAILED': {
                'Subject': f"Error {account['OperationalStatus'].lower()} AWS account {account['AwsAccountName']}",
                'Body': f"There was a problem {account['OperationalStatus'].lower()} the AWS account {account['AwsAccountName']}: {additional_messages}"
            }
        },
        'UPDATE_START': {
            'SUCCEEDED': {
                'Subject': f"AWS account {account['AwsAccountName']} has gone into the {account['OperationalStatus'].lower()} state",
                'Body': f"AWS account {account['AwsAccountName']} has gone into the {account['OperationalStatus'].lower()} state"
            },
            'FAILED': {
                'Subject': f"Error {account['OperationalStatus'].lower()} AWS account {account['AwsAccountName']}",
                'Body': f"There was a problem {account['OperationalStatus'].lower()} the AWS account {account['AwsAccountName']}: {additional_messages}"
            }
        },
        'UNENROL_START': {
            'SUCCEEDED': {
                'Subject': f"AWS account {account['AwsAccountName']} has gone into the {account['OperationalStatus'].lower()} state",
                'Body': f"AWS account {account['AwsAccountName']} has gone into the {account['OperationalStatus'].lower()} state"
            },
            'FAILED': {
                'Subject': f"Error {account['OperationalStatus'].lower()} AWS account {account['AwsAccountName']}",
                'Body': f"There was a problem {account['OperationalStatus'].lower()} the AWS account {account['AwsAccountName']}: {additional_messages}"
            }
        },
        'PROVISION_END': {
            'SUCCEEDED': {
                'Subject': f"AWS account {account['AwsAccountName']} ({account['AwsAccountId']}) is ready",
                'Body': f"AWS account {account['AwsAccountName']} ({account['AwsAccountId']}) is ready. Please set up MFA."
            },
            'FAILED': {
                'Subject': f"AWS account {account['AwsAccountName']} ({account['AwsAccountId']}) was not successfully provisioned / enrolled",
                'Body': f"AWS account {account['AwsAccountName']} ({account['AwsAccountId']}) was not successfully provisioned / enrolled"
            }
        },
        'ENROL_END': {
            'SUCCEEDED': {
                'Subject': f"AWS account {account['AwsAccountName']} ({account['AwsAccountId']}) is ready",
                'Body': f"AWS account {account['AwsAccountName']} ({account['AwsAccountId']}) is ready. Please set up MFA."
            },
            'FAILED': {
                'Subject': f"AWS account {account['AwsAccountName']} ({account['AwsAccountId']}) was not successfully provisioned / enrolled",
                'Body': f"AWS account {account['AwsAccountName']} ({account['AwsAccountId']}) was not successfully provisioned / enrolled"
            }
        },
        'UPDATE_END': {
            'SUCCEEDED': {
                'Subject': f"AWS account {account['AwsAccountName']} ({account['AwsAccountId']}) has been updated",
                'Body': f"AWS account {account['AwsAccountName']} ({account['AwsAccountId']}) was recently updated."
            },
            'FAILED': {
                'Subject': f"AWS account {account['AwsAccountName']} ({account['AwsAccountId']}) was not successfully updated",
                'Body': f"AWS account {account['AwsAccountName']} ({account['AwsAccountId']}) was not successfully updated"
            }
        }
    }

    return templates[action][state]


def send_notification(action, state, account, additional_messages = None):
    """Sends e-mail notifications using AWS Simple E-mail Service to provide
    updates on the status of processing accounts in Control Tower.

    Parameters:
    ----------
    - action [string]: The action being taken. Accepted values are 'PROVISION_START', 'PROVISION_END', 'ENROL_START', 'ENROL_END',
        'UPDATE_START', 'UPDATE_END' and 'UNENROL_START'.
    - state [string]: The state of the process. Accepted values are 'SUCCEEDED' and 'FAILED'.
    - account [dictionary]: A dictionary containing key / value pairs corresponding to the details of the account.
    - additional_messages [string][optional]: Any additional messages that are needed to be sent.

    Return:
    ------
    None
    """

    template = __get_template(action, state, account, additional_messages)

    message_id = ses.send_email(
        FromEmailAddress = from_address,
        FromEmailAddressIdentityArn = ses_identity_arn,
        Destination = {
            'ToAddresses': [ account['AwsAccountRootAddress'] ]
        },
        Content = {
            'Simple': {
                'Subject': {
                    'Data': template['Subject']
                },
                'Body': {
                    'Text': {
                        'Data': template['Body']
                    }
                }
            }
        }
    )['MessageId']

    logger.debug(f'Notification sent using SES with receipt {message_id}')

    return