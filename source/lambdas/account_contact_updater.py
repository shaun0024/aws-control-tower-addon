"""Common modules"""
import logging
from os import environ

"""Modules required by this file"""
import json
from boto3 import client, Session

"""Logging settings"""
logger = logging.getLogger()
log_level = 'INFO' if 'LOGGING_LEVEL' not in environ else environ['LOGGING_LEVEL'].upper()
level = logging.getLevelName(log_level)
logger.setLevel(level)

"""Global clients and variables"""
role_to_assume = environ['IAM_ROLE_ARN']
ssm_parameter_names = {
    'PRIMARY': f"/{environ['SSM_PARAMETER_PATH']}/primary",
    'BILLING': f"/{environ['SSM_PARAMETER_PATH']}/billing",
    'OPERATIONS': f"/{environ['SSM_PARAMETER_PATH']}/operations",
    'SECURITY': f"/{environ['SSM_PARAMETER_PATH']}/security"
}

ssm = client('ssm')
sts = client('sts')

assumed_role = sts.assume_role(
    RoleArn = role_to_assume,
    RoleSessionName = 'AccountManagement'
)['Credentials']

session = Session(
    aws_access_key_id = assumed_role['AccessKeyId'],
    aws_secret_access_key = assumed_role['SecretAccessKey'],
    aws_session_token = assumed_role['SessionToken']
)

account = session.client('account')



def lambda_handler(event, context):
    """The entrypoint for the Lambda.

    Parameters:
    ----------
    - event [dictionary]: The event received.
    - context [dictionary]: The context of the event.

    Return:
    ------
    None
    """

    logger.debug(f'Received event: {event}')
    logger.debug(f'Received context: {context}')

    controller(event)

    return



def controller(event):
    """The router that iterates through the records from SNS and forwards  for further processing.

    Parameters:
    ----------
    - event [dictionary]: The event received.

    Return:
    ------
    None
    """

    for index, record in enumerate(event['Records']):
        try:
            process_record(json.loads(record['Sns']['Message']))

        except Exception as error:
            logger.exception(f'Failed to process record: {record}')

    return



def process_record(record):
    """Evaluates the SNS record and updates the contact details if the criterias are met.

    Parameters:
    ----------
    - record [dictionary]: The message in the SNS record for processing.

    Return:
    ------
    None
    """

    aws_account_id = None

    if 'source' not in record:
        return

    if record['source'] != 'aws.controltower':
        return

    if record['detail']['eventName'] != 'CreateManagedAccount':
        return

    if (record['detail']['eventName'] == 'CreateManagedAccount' and \
        record['detail']['serviceEventDetails']['createManagedAccountStatus']['state'] == 'SUCCEEDED'):
        aws_account_id = record['detail']['serviceEventDetails']['createManagedAccountStatus']['account']['accountId']

    if aws_account_id is None:
        return

    update_contact(aws_account_id)
    update_alternate_contacts(aws_account_id)

    return



def update_contact(aws_account_id):
    """Checks the primary contact in the provided account and updates it if incorrect.

    Parameters:
    ----------
    - aws_account_id [string]: The id of the account to update.

    Return:
    ------
    None
    """

    primary_contact = json.loads(ssm.get_parameter(Name = ssm_parameter_names['PRIMARY'])['Parameter']['Value'])

    current_contact = account.get_contact_information(
        AccountId = aws_account_id
    )['ContactInformation']

    logger.debug(f'Current values found for primary contact: {current_contact}')

    for detail in primary_contact:
        if current_contact[detail] != primary_contact[detail]:
            logger.info(f'Current values for primary contact are incorrect, updating')
            logger.debug(f"Updating values for primary contact to: {primary_contact}")

            account.put_contact_information(
                AccountId = aws_account_id,
                ContactInformation = primary_contact
            )

            logger.info(f'Values for primary contact updated')
            break

    return



def update_alternate_contacts(aws_account_id):
    """Checks the alternate contacts in the provided account and updates it if incorrect.

    Parameters:
    ----------
    - aws_account_id [string]: The id of the account to update.

    Return:
    ------
    None
    """

    def update_contact_details(aws_account_id, alternate_contact_type, alternate_contact):
        """Update the alternate contacts in the provided account.

        Parameters:
        ----------
        - aws_account_id [string]: The id of the account to update.
        - alternate_contact_type [string]: The alternate contact type. This can be BILLING, OPERATIONS or SECURITY.
        - alternate_contact [dictionary]: A dictionary with the key value of Name, Title, EmailAddress and PhoneNumber.

        Return:
        ------
        None
        """

        account.put_alternate_contact(
            AccountId = aws_account_id,
            AlternateContactType = alternate_contact_type,
            Name = alternate_contact['Name'],
            Title = alternate_contact['Title'],
            EmailAddress = alternate_contact['EmailAddress'],
            PhoneNumber = alternate_contact['PhoneNumber']
        )

        logger.info(f'Values for {alternate_contact_type} contact updated')

        return


    for alternate_contact_type in ssm_parameter_names:
        if alternate_contact_type != 'PRIMARY':
            alternate_contact = json.loads(ssm.get_parameter(Name = ssm_parameter_names[alternate_contact_type])['Parameter']['Value'])

            try:
                current_alternate_contact = account.get_alternate_contact(
                    AccountId = aws_account_id,
                    AlternateContactType = alternate_contact_type
                )['AlternateContact']

                logger.debug(f'Current values found for {alternate_contact_type} contact: {current_alternate_contact}')

                for detail in alternate_contact:
                    if current_alternate_contact[detail] != alternate_contact[detail]:
                        logger.info(f'Current values for {alternate_contact_type} contact are incorrect, updating')
                        logger.debug(f"Updating values for {alternate_contact_type} contact to: {alternate_contact}")

                        update_contact_details(aws_account_id, alternate_contact_type, alternate_contact)

                        break

            except Exception as error:
                if error.response['Error']['Code'] == 'ResourceNotFoundException':
                    logger.info(f'No current values for {alternate_contact_type} contact, updating')
                    logger.debug(f"Updating values for {alternate_contact_type} contact to: {alternate_contact}")

                    update_contact_details(aws_account_id, alternate_contact_type, alternate_contact)

                else:
                    raise error

    return