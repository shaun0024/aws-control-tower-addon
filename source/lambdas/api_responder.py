"""Common modules"""
import logging
from os import environ

"""Modules required by this file"""
import account_register
import json
import re
from boto3 import client, Session
from validator import get_common_pattern, validate_input_schema

"""Logging settings"""
logger = logging.getLogger()
log_level = 'INFO' if 'LOGGING_LEVEL' not in environ else environ['LOGGING_LEVEL'].upper()
level = logging.getLevelName(log_level)
logger.setLevel(level)

"""Global variables"""
product_id = environ['PRODUCT_ID']
queue_url = environ['QUEUE_URL']
role_to_assume = environ['IAM_ROLE_ARN']

"""Global clients"""
sqs = client('sqs')
sts = client('sts')

assumed_role = sts.assume_role(
    RoleArn = role_to_assume,
    RoleSessionName = 'AccountManagement'
)['Credentials']

session = Session(
    aws_access_key_id = assumed_role['AccessKeyId'],
    aws_secret_access_key = assumed_role['SecretAccessKey'],
    aws_session_token = assumed_role['SessionToken']
)

organizations = session.client('organizations')



def lambda_handler(event, context):
    """The entrypoint for the Lambda.

    Parameters:
    ----------
    - event [dictionary]: The event received.
    - context [dictionary]: The context of the event.

    Return:
    ------
    None
    """

    logger.debug(f'Received event: {event}')
    logger.debug(f'Received context: {context}')

    try:
        response = controller(event)
    except Exception as e:
        response = format_response(
            'UNKNOWN_ERROR',
            {
                'Error': f'An unknown error has occured: {e}',
                'LambdaRequestId': context.aws_request_id,
                'LogGroupName': context.log_group_name,
                'LogStreamName': context.log_stream_name
            }
        )

    return {
        'headers': {
            'Content-Type': 'application/json'
        },
        'statusCode': response['StatusCode'],
        'body': json.dumps(response['Body'])
    }



def controller(event):
    """The router that forwards the event for further processing.

    Parameters:
    ----------
    - event [dictionary]: The event received.

    Return:
    ------
    dictionary
    """

    if event['pathParameters'] is not None:
        for path_parameter in event['pathParameters']:
            regex_pattern = get_common_pattern(path_parameter)
            pattern = re.compile(regex_pattern)
            if not pattern.match(event['pathParameters'][path_parameter]):
                return format_response(
                    'PATH_PARAMETER_VALIDATION_FAILED',
                    { 'Error': f"Path parameter {path_parameter} does not match pattern '{regex_pattern}'" }
                )

    if event['httpMethod'] == 'GET':
        action = 'DESCRIBE'
        response = describe_account(event['pathParameters']['AwsAccountId'])

    elif event['resource'] == '/aws/account' and event['httpMethod'] == 'POST':
        action = 'PROVISION'
        response = format_payload(action, json.loads(event['body']))

    elif event['resource'] == '/aws/account/{AwsAccountId}' and event['httpMethod'] == 'POST':
        action = 'ENROL'
        response = format_payload(action, json.loads(event['body']), event['pathParameters']['AwsAccountId'])

    elif event['httpMethod'] == 'PATCH':
        action = 'UPDATE'
        response = format_payload(action, json.loads(event['body']), event['pathParameters']['AwsAccountId'])

    elif event['httpMethod'] == 'DELETE':
        action = 'UNENROL'
        response = format_payload(action, None, event['pathParameters']['AwsAccountId'])

    if 'QUEUED' in response['Body']['Status']:
        message_id = queue_account(action, response['Body']['Account'])
        response['Body'].update({ 'MessageId': message_id })

    return response



def describe_account(aws_account_id):
    account = account_register.get_account_by_id(AwsAccountId = aws_account_id)

    if account is None:
        return format_response(
            'AWS_ACCOUNT_NOT_FOUND',
            { 'Error': f'AWS account {aws_account_id} not found' }
        )

    else:
        return format_response(
            'AWS_ACCOUNT_FOUND',
            { 'Account': account }
        )



def format_payload(action, payload, aws_account_id = None):
    """Updates details of the account in the account register.

    Parameters:
    ----------
    - action [string]: The action to take. Accepted values are 'PROVISION', 'ENROL' and 'UPDATE'.
    - payload [dictionary]: The contents of the payload received from the AWS API Gateway.
    - aws_account_id [string]: The id of the AWS account.

    Return:
    ------
    None
    """

    def __get_organizational_unit_name(organizational_unit_id):
        """Gets the AWS Organizational Unit name and returns in the format required for
        provisioning / enrollment / updating (e.g. 'OU-NAME (ou-id)').

        Parameters:
        ----------
        - organizational_unit_id [string]: The AWS Organizational Unit id.

        Return:
        ------
        string
        """

        organizational_unit = organizations.describe_organizational_unit(OrganizationalUnitId = organizational_unit_id)['OrganizationalUnit']
        organizational_unit_name = f"{organizational_unit['Name']} ({organizational_unit['Id']})"

        return organizational_unit_name


    def __get_existing_account_details(aws_account_id):
        """Gets details of an existing AWS account registered in Organizations using the Organizations API.

        Parameters:
        ----------
        - aws_account_id [string]: The id of the AWS account.

        Return:
        ------
        None
        """

        account_details = organizations.describe_account(AccountId = aws_account_id)['Account']

        return account_details



    if action != 'UNENROL':
        try:
            validate_input_schema(action, payload)

        except Exception as e:
            return format_response(
                'BODY_VALIDATION_FAILED',
                { 'Error': str(e) }
            )

    call_service_catalog = True

    if action == 'PROVISION':
        if account_register.get_item_by_root_address(AwsAccountRootAddress = payload['AwsAccountRootAddress']) is not None:
            return format_response(
                'AWS_ACCOUNT_ROOT_ADDRESS_IN_USE',
                { 'Error': f"Root address {payload['AwsAccountRootAddress']} used for existing account" }
            )

        account = account_register.get_account_by_name(AwsAccountName = payload['AwsAccountName'])

    else:
        account = account_register.get_account_by_id(AwsAccountId = aws_account_id)


    if action in [ 'PROVISION', 'ENROL' ] and account is not None:
        return format_response(
            'AWS_ACCOUNT_EXISTS',
            { 'Error': f"AWS account {account['AwsAccountName']} ({account['AwsAccountId']}) already exists" }
        )

    elif action in [ 'UPDATE', 'UNENROL' ] and account is None:
        return format_response(
            'AWS_ACCOUNT_NOT_FOUND',
            { 'Error': f"AWS account {aws_account_id} not found" }
        )


    if action in [ 'PROVISION', 'ENROL' ]:
        payload.update({
            'AwsOrganisationalUnitId': __get_organizational_unit_name(payload['AwsOrganisationalUnitId']),
            'ProductId': product_id,
            'ProvisionedProductName': f"Launch-Account-{payload['AwsAccountName']}" if action == 'PROVISION' else f'Enroll-Account-{aws_account_id}',
            'OperationalStatus': 'QUEUED_FOR_PROVISIONING' if action == 'PROVISION' else 'QUEUED_FOR_ENROLMENT'
        })

        if action == 'ENROL':
            account_details = __get_existing_account_details(aws_account_id)

            payload.update({
                'AwsAccountId': aws_account_id,
                'AwsAccountName': account_details['Name'],
                'AwsAccountRootAddress': account_details['Email'],
                'AwsAccountSSOAddress': account_details['Email'],
                'AwsAccountSSOFirstName': 'Admin',
                'AwsAccountSSOLastName': 'User'
            })

        account_register.add_account_to_register(AwsAccount = payload)

        return format_response(
            payload['OperationalStatus'],
            { 'Account': payload }
        )


    elif action == 'UPDATE':
        update_register = False
        call_service_catalog = False
        supported_change_attributes = [ 'AwsOrganisationalUnitId', 'AwsAccountSSOAddress', 'AwsAccountSSOFirstName', 'AwsAccountSSOLastName' ]

        for attribute_to_update in payload:
            if (attribute_to_update in supported_change_attributes and attribute_to_update == 'AwsOrganisationalUnitId'):
                organizational_unit_name = __get_organizational_unit_name(payload['AwsOrganisationalUnitId'])

                if account['AwsOrganisationalUnitId'] != organizational_unit_name:
                    payload.update({ 'AwsOrganisationalUnitId': organizational_unit_name })
                    account['AwsOrganisationalUnitId'] = organizational_unit_name
                    update_register = True
                    call_service_catalog = True

            elif (attribute_to_update in supported_change_attributes and attribute_to_update != 'AwsOrganisationalUnitId'):
                if payload[attribute_to_update] != account[attribute_to_update]:
                    account[attribute_to_update] = payload[attribute_to_update]
                    update_register = True
                    call_service_catalog = True

            elif payload[attribute_to_update] != account[attribute_to_update]:
                account[attribute_to_update] = payload[attribute_to_update]
                update_register = True


        if call_service_catalog is True:
            payload.update({ 'OperationalStatus': 'QUEUED_FOR_UPDATE' })


        if update_register is True:
            account_register.update_account(
                AwsAccountName = account['AwsAccountName'],
                AwsAccountId = account['AwsAccountId'],
                AttributesToUpdate = payload
            )


        return format_response(
            'AWS_ACCOUNT_UPDATED' if call_service_catalog is False else payload['OperationalStatus'],
            { 'Account': account }
        )


    elif action == 'UNENROL':
        account.update({ 'OperationalStatus': 'QUEUED_FOR_UNENROLMENT' })

        account_register.update_account(
            AwsAccountName = account['AwsAccountName'],
            AwsAccountId = account['AwsAccountId'],
            AttributesToUpdate = { 'OperationalStatus': 'QUEUED_FOR_UNENROLMENT' }
        )

        return format_response(
            account['OperationalStatus'],
            { 'Account': account }
        )



def queue_account(action, account):
    """Queues the account for processing by the Control Tower Account Factory product in the Service Catalog.

    Parameters:
    ----------
    - action [string]: The action to take. Accepted values are 'PROVISION', 'ENROL' and 'UPDATE'.
    - account [dictionary]: The details of the account.

    Return:
    ------
    string
    """

    message_id = sqs.send_message(
        QueueUrl = queue_url,
        MessageGroupId = 'AwsControlTowerAccountFactory',
        MessageBody = json.dumps(account),
        MessageAttributes = {
            'Operation': {
                'DataType': 'String',
                'StringValue': action
            }
        }
    )['MessageId']

    return message_id



def format_response(status, additional_attributes = None):
    """Formats the response to return to the API Gateway.

    Parameters:
    ----------
    - status [string]: The status for the response..
    - additional_attributes [dictionary]: Key / value pairs of additional information to include in the response.

    Return:
    ------
    string
    """

    status_map = {
        'AWS_ACCOUNT_FOUND': 200,
        'AWS_ACCOUNT_UPDATED': 200,
        'QUEUED_FOR_PROVISIONING': 200,
        'QUEUED_FOR_ENROLMENT': 200,
        'QUEUED_FOR_UPDATE': 200,
        'QUEUED_FOR_UNENROLMENT': 200,
        'AWS_ACCOUNT_ROOT_ADDRESS_IN_USE': 400,
        'AWS_ACCOUNT_NOT_FOUND': 404,
        'AWS_ACCOUNT_EXISTS': 400,
        'PATH_PARAMETER_VALIDATION_FAILED': 400,
        'BODY_VALIDATION_FAILED': 400,
        'UNKNOWN_ERROR': 400
    }

    response = {
        'StatusCode': status_map[status],
        'Body': {
            'Status': status
        }
    }

    if additional_attributes is not None:
        response['Body'].update(additional_attributes)

    return response