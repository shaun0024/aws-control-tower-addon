"""Common modules"""
import logging
from os import environ

"""Modules required by this file"""
import account_register
import json
from boto3 import client
from notification import send_notification

"""Logging settings"""
logger = logging.getLogger()
log_level = 'INFO' if 'LOGGING_LEVEL' not in environ else environ['LOGGING_LEVEL'].upper()
level = logging.getLevelName(log_level)
logger.setLevel(level)

"""Global variables"""
from_address = environ['FROM_ADDRESS']
ses_identity_arn = environ['SES_IDENTITY_ARN']

"""Global clients"""
ses = client('sesv2')



def lambda_handler(event, context):
    """The entrypoint for the Lambda.

    Parameters:
    ----------
    - event [dictionary]: The event received.
    - context [dictionary]: The context of the event.

    Return:
    ------
    None
    """

    logger.debug(f'Received event: {event}')
    logger.debug(f'Received context: {context}')

    controller(event)

    return



def controller(event):
    """The router that forwards the event for further processing.

    Parameters:
    ----------
    - event [dictionary]: The event received.

    Return:
    ------
    None
    """

    for record in event['Records']:
        try:
            process_record(json.loads(record['Sns']['Message']))

        except Exception as error:
            logger.exception(f'Failed to process record: {record}')

    return



def process_record(record):
    """Process the individual records in the SNS message and
    updates the AWS Account Register with the status.

    Parameters:
    ----------
    - record [dictionary]: Record from the SNS message.

    Return:
    ------
    None
    """

    aws_account_id = None
    aws_account_name = None

    if 'source' not in record:
        return

    if record['source'] != 'aws.controltower':
        return

    if record['detail']['eventName'] not in [ 'CreateManagedAccount', 'UpdateManagedAccount' ]:
        return

    event_type = record['detail']['eventName']

    if event_type == 'CreateManagedAccount':
        aws_account_id = record['detail']['serviceEventDetails']['createManagedAccountStatus']['account']['accountId']
        aws_account_name = record['detail']['serviceEventDetails']['createManagedAccountStatus']['account']['accountName']
        state = record['detail']['serviceEventDetails']['createManagedAccountStatus']['state']

    elif event_type == 'UpdateManagedAccount':
        aws_account_id = record['detail']['serviceEventDetails']['updateManagedAccountStatus']['account']['accountId']
        aws_account_name = record['detail']['serviceEventDetails']['updateManagedAccountStatus']['account']['accountName']
        state = record['detail']['serviceEventDetails']['updateManagedAccountStatus']['state']


    if event_type == 'CreateManagedAccount':
        status = 'ACTIVE' if state == 'SUCCEEDED' else 'FAILED_TO_PROVISION_OR_ENROLL'

    elif event_type == 'UpdateManagedAccount':
        status = 'ACTIVE' if state == 'SUCCEEDED' else 'FAILED_TO_UPDATE'


    if (aws_account_id is None and aws_account_name is None):
        logger.error(f'Failed to identify AWS account id and alias, unable to update register')
        return


    account = account_register.update_account(
        AwsAccountName = aws_account_name,
        AwsAccountId = aws_account_id,
        AttributesToUpdate = { 'OperationalStatus': status }
    )

    if event_type == 'CreateManagedAccount':
        action = 'PROVISION' if 'Launch' in account['ProvisionedProductName'] else 'ENROL'

    else:
        action = 'UPDATE'

    # send_notification(account['AwsAccountRootAddress'], subject, body)
    send_notification(f'{action}_END', state, account)

    return