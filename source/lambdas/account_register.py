"""Common modules"""
import logging
from os import environ

"""Modules required by this file"""
import json
import pytz
from boto3 import resource
from datetime import datetime

"""Logging settings"""
logger = logging.getLogger()
log_level = 'INFO' if 'LOGGING_LEVEL' not in environ else environ['LOGGING_LEVEL'].upper()
level = logging.getLevelName(log_level)
logger.setLevel(level)

"""Global variables"""
table = environ['TABLE_NAME']
table_account_id_index = environ['TABLE_ACCOUNT_ID_INDEX']
table_root_address_index = environ['TABLE_ROOT_ADDRESS_INDEX']

"""Global clients"""
dynamo = resource('dynamodb').Table(table)



def add_account_to_register(**kwargs):
    """Adds a new AWS account to the register.

    Arguments:
    ----------
    - AwsAccount [dictionary]: A dictionary containing key / value pairs corresponding to the details of the account.

    Return:
    ------
    None
    """

    if 'AwsAccount' not in kwargs:
        raise Exception('AwsAccount was not provided')

    aws_account = kwargs.get('AwsAccount')
    aws_account.update({
        'CreatedOn': datetime.strftime(datetime.now(pytz.utc), '%Y-%m-%dT%H:%M:%SZ'),
        'LastUpdateOn': datetime.strftime(datetime.now(pytz.utc), '%Y-%m-%dT%H:%M:%SZ')
    })

    dynamo.put_item(Item = aws_account)

    return



def get_account_by_name(**kwargs):
    """Gets the details of the AWS account based on the account name from the register.

    Arguments:
    ----------
    - AwsAccountName [string]: The name of the AWS account.

    Return:
    ------
    dictionary
    """

    if 'AwsAccountName' not in kwargs:
        raise Exception('AwsAccountName was not provided')

    aws_account_name = kwargs.get('AwsAccountName')

    query = {
        'TableName': table,
        'Key': {
            'AwsAccountName': aws_account_name
        }
    }

    result = dynamo.get_item(**query)

    if 'Item' in result:
        account = result['Item']
    else:
        account = None

    return account



def get_account_by_id(**kwargs):
    """Gets the details of the AWS account based on the account id from the register.

    Arguments:
    ----------
    - AwsAccountId [string]:  The id of the AWS account.
    - AwsAccountName [string][optional]: The name of the AWS account.

    Return:
    ------
    dictionary
    """

    if 'AwsAccountId' not in kwargs:
        raise Exception('AwsAccountId was not provided')

    aws_account_id = kwargs.get('AwsAccountId')
    aws_account_name = kwargs.get('AwsAccountName', None)

    query = {
        'TableName': table,
        'IndexName': table_account_id_index,
        'KeyConditionExpression': 'AwsAccountId = :aws_account_id',
        'ExpressionAttributeValues': {
            ':aws_account_id': aws_account_id
        }
    }

    if aws_account_name is not None:
        query.update({ 'FilterExpression': 'AwsAccountName = :aws_account_name' })
        query['ExpressionAttributeValues'].update({ ':aws_account_name': aws_account_name })

    results = dynamo.query(**query)

    if results['Count'] == 1:
        account = results['Items'][0]
    else:
        account = None

    return account



def get_item_by_root_address(**kwargs):
    """Gets the details of the AWS account based on the root address from the register.

    Arguments:
    ----------
    - AwsAccountRootAddress [string]:  The root address of the AWS account.

    Return:
    ------
    dictionary
    """

    if 'AwsAccountRootAddress' not in kwargs:
        raise Exception('AwsAccountRootAddress was not provided')

    aws_root_address = kwargs.get('AwsAccountRootAddress')

    query = {
        'TableName': table,
        'IndexName': table_root_address_index,
        'KeyConditionExpression': 'AwsAccountRootAddress = :aws_root_address',
        'ExpressionAttributeValues': {
            ':aws_root_address': aws_root_address
        }
    }

    results = dynamo.query(**query)

    if results['Count'] == 1:
        account = results['Items'][0]
    else:
        account = None

    return account



def update_account(**kwargs):
    """Updates the details of the AWS account in the register.

    Arguments:
    ----------
    - AwsAccountName [string]: The name of the AWS account.
    - AwsAccountId [string]: The id of the AWS account.
    - AttributesToUpdate [dictionary]: A set of keys and associated values that are to be updated.
    - AttributesToRemove [list of strings]: A list of strings representing attributes to be removed.

    Return:
    ------
    dictionary
    """

    if 'AwsAccountName' not in kwargs:
        raise Exception('AwsAccountName was not provided')

    aws_account_name = kwargs.get('AwsAccountName')
    aws_account_id = kwargs.get('AwsAccountId', None)
    attributes_to_update = kwargs.get('AttributesToUpdate', None)
    attributes_to_remove = kwargs.get('AttributesToRemove', None)

    query = {
        'Key': {
            'AwsAccountName': aws_account_name
        },
        'ReturnValues': 'ALL_NEW',
        'ExpressionAttributeValues': {
            ':last_update_on': datetime.strftime(datetime.now(pytz.utc), '%Y-%m-%dT%H:%M:%SZ')
        }
    }

    if aws_account_id is not None:
        query.update({ 'UpdateExpression': 'set AwsAccountId = :aws_account_id, LastUpdateOn = :last_update_on' })
        query['ExpressionAttributeValues'].update({ ':aws_account_id': aws_account_id })

    else:
        query.update({ 'UpdateExpression': 'set LastUpdateOn = :last_update_on' })

    if attributes_to_update is not None:
        for attribute in attributes_to_update:
            query['UpdateExpression'] = f"{query['UpdateExpression']}, {attribute} = :{attribute}"
            query['ExpressionAttributeValues'].update({ f':{attribute}': attributes_to_update[attribute] })

    if attributes_to_remove is not None:
        query['UpdateExpression'] = f"{query['UpdateExpression']} remove {', '.join(attributes_to_remove)}"

    account = dynamo.update_item(**query)['Attributes']

    return account


def delete_account(**kwargs):
    """Deletes the AWS account from the register.

    Arguments:
    ----------
    - AwsAccountName [string]: The name of the AWS account.

    Return:
    ------
    dictionary
    """

    if 'AwsAccountName' not in kwargs:
        raise Exception('AwsAccountName was not provided')

    aws_account_name = kwargs.get('AwsAccountName')

    query = {
        'Key': {
            'AwsAccountName': aws_account_name
        }
    }

    dynamo.delete_item(**query)

    return