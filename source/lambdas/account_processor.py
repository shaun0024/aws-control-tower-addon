"""Common modules"""
import logging
from os import environ

"""Modules required by this file"""
import account_register
import json
from boto3 import client, Session
from notification import send_notification
from time import sleep

"""Logging settings"""
logger = logging.getLogger()
log_level = 'INFO' if 'LOGGING_LEVEL' not in environ else environ['LOGGING_LEVEL'].upper()
level = logging.getLevelName(log_level)
logger.setLevel(level)

"""Global variables"""
from_address = environ['FROM_ADDRESS']
interval_in_seconds = 45 if 'INTERVAL_IN_SECONDS' not in environ else int(environ['INTERVAL_IN_SECONDS'])
number_of_checks = 3 if 'NUMBER_OF_CHECKS' not in environ else int(environ['NUMBER_OF_CHECKS'])
product_id = environ['PRODUCT_ID']
queue_url = environ['QUEUE_URL']
role_to_assume = environ['IAM_ROLE_ARN']
ses_identity_arn = environ['SES_IDENTITY_ARN']

"""Global clients"""
ses = client('sesv2')
sqs = client('sqs')
sts = client('sts')

assumed_role = sts.assume_role(
    RoleArn = role_to_assume,
    RoleSessionName = 'AccountManagement'
)['Credentials']

session = Session(
    aws_access_key_id = assumed_role['AccessKeyId'],
    aws_secret_access_key = assumed_role['SecretAccessKey'],
    aws_session_token = assumed_role['SessionToken']
)

organizations = session.client('organizations')
service_catalog = session.client('servicecatalog')



def lambda_handler(event, context):
    """The entrypoint for the Lambda.

    Parameters:
    ----------
    - event [dictionary]: The event received.
    - context [dictionary]: The context of the event.

    Return:
    ------
    None
    """

    logger.debug(f'Received event: {event}')
    logger.debug(f'Received context: {context}')

    controller()

    return



def controller():
    """Initiates the processing by calling other functions.

    Parameters:
    ----------
    None

    Return:
    ------
    None
    """

    if process_queue() is False:
        return

    action, account = retrieve_message_from_queue()

    if action is None and account is None:
        return

    manage_product(action, account)

    return



def process_queue():
    """Checks if there are AWS Control Tower Account Factory products in the Service Catalog are
    being provisioned, updated or terminated.

    Parameters:
    ----------
    None

    Return:
    ------
    None
    """

    service_catalog_parameters = {
        'AccessLevelFilter': {
            'Key': 'Account',
            'Value': 'self'
        },
        'Filters': {
            'SearchQuery': [ f'productId:{product_id}' ]
        }
    }

    provisioned_products = []

    while True:
        response = service_catalog.search_provisioned_products(**service_catalog_parameters)
        provisioned_products.extend(response['ProvisionedProducts'])

        if 'NextPageToken' in response:
            service_catalog_parameters.update({ 'PageToken': response['NextPageToken']})

        else:
            break

    provisioned_products_in_progress = [
        provisioned_product for provisioned_product in provisioned_products \
            if provisioned_product['Status'] in [ 'PLAN_IN_PROGRESS', 'UNDER_CHANGE' ]
    ]

    if len(provisioned_products_in_progress) == 0:
        logger.info('No accounts are being provisioned / enrolled / updated, ready to process next item in queue')
        return True

    else:
        logger.info('One or more accounts are currently being provisioned / enrolled / updated')
        return False



def retrieve_message_from_queue():
    """Processes messages in the SQS queue used for account management.

    Parameters:
    ----------
    None

    Return:
    ------
    None
    """

    action = None
    account = None
    # receipt_handle = None

    messages = sqs.receive_message(
        QueueUrl = queue_url,
        MaxNumberOfMessages = 1,
        MessageAttributeNames = [ 'Operation' ],
        VisibilityTimeout = 3600
    )

    if 'Messages' in messages and len(messages['Messages']) > 0:
        logger.debug(f"Message received: {messages['Messages'][0]}")

        action = messages['Messages'][0]['MessageAttributes']['Operation']['StringValue']
        account = json.loads(messages['Messages'][0]['Body'])
        # receipt_handle = messages['Messages'][0]['ReceiptHandle']

        sqs.delete_message(
            QueueUrl = queue_url,
            ReceiptHandle = messages['Messages'][0]['ReceiptHandle']
        )

    else:
        logger.info('No messages in queue')

    return action, account



def manage_product(action, account):
    """Provisions a new account, enrols an existing account, updates a managed account or unenrols a managed account
    through the AWS Control Tower Account Factory product in the Service Catalog.

    Parameters:
    ----------
    - action [stting]: The action to take on the account. Accepted values are PROVISION, ENROL, UPDATE or UNENROL.
    - account [dictionary]: The details of the account to provision, enrol, update or unenrol.

    Return:
    ------
    None
    """


    def __get_product_artifact_id():
        """Gets the latest version (i.e. artifact) of the AWS Control Tower Account Factory
        product in the Service Catalog.

        Parameters:
        ----------
        None

        Return:
        ------
        None
        """

        provisioning_artifacts = service_catalog.describe_product_as_admin(Id = product_id)['ProvisioningArtifactSummaries']

        if len(provisioning_artifacts) > 0:
            provisioning_artifact_id = provisioning_artifacts[-1]['Id']

        else:
            raise Exception(f"No provisioning artifact / version found for product {provisioning_parameters['ProductId']}")

        return provisioning_artifact_id


    def __check_provisioning_status(provisioned_product_id):
        """Checks the initial minutes of the provisioning, enrolment, update or unenrolment process of the
        AWS Control Tower Account Factory product in the Service Catalog to determine if it fails.

        Parameters:
        ----------
        provisioned_product_id [string]: The id of the provisioned AWS Control Tower Account Factory product.

        Return:
        ------
        string (or None if no failures detected during the initial minutes)
        """

        status_message = None

        for i in range(0, number_of_checks):
            sleep(interval_in_seconds)

            provisioned_product_detail = service_catalog.describe_provisioned_product(Id = provisioned_product_id)['ProvisionedProductDetail']

            if provisioned_product_detail['Status'] in [ 'ERROR', 'TAINTED' ]:
                status_message = provisioned_product_detail['StatusMessage']
                logger.error(f'Failed to {action.lower()} account with product id {provisioned_product_id} with error: {status_message}')
                break

        return status_message


    status_action_map = {
        'PROVISION': 'PROVISIONING',
        'ENROL': 'ENROLLING',
        'UPDATE': 'UPDATING',
        'UNENROL': 'UNENROLLING'
    }

    account['OperationalStatus'] = status_action_map[action]

    if action != 'UNENROL':
        provisioning_parameters = {
            'ProductId': product_id,
            'ProvisioningParameters': [
                {
                    'Key': 'AccountName',
                    'Value': account['NewAwsAccountName'] if 'NewAwsAccountName' in account else account['AwsAccountName']
                },
                {
                    'Key': 'AccountEmail',
                    'Value': account['AwsAccountRootAddress']
                },
                {
                    'Key': 'ManagedOrganizationalUnit',
                    'Value': account['AwsOrganisationalUnitId']
                },
                {
                    'Key': 'SSOUserEmail',
                    'Value': account['AwsAccountSSOAddress']
                },
                {
                    'Key': 'SSOUserFirstName',
                    'Value': account['AwsAccountSSOFirstName']
                },
                {
                    'Key': 'SSOUserLastName',
                    'Value': account['AwsAccountSSOLastName']
                }
            ]
        }

        provisioning_artifact_id = __get_product_artifact_id()

        provisioning_parameters.update({ 'ProvisioningArtifactId': provisioning_artifact_id })


        if action == 'PROVISION' or action == 'ENROL':
            provisioning_parameters.update({ 'ProvisionedProductName': account['ProvisionedProductName'] })

        else:
            provisioning_parameters.update({ 'ProvisionedProductId': account['ProvisionedProductId'] })

        logger.debug(f"{account['OperationalStatus'].capitalize()} account using the following parameters: {provisioning_parameters}")


        if action == 'UPDATE':
            response = service_catalog.update_provisioned_product(**provisioning_parameters)

        else:
            response = service_catalog.provision_product(**provisioning_parameters)


        account.update({
            'ProductVersion': provisioning_artifact_id,
            'ProvisionedProductId': response['RecordDetail']['ProvisionedProductId']
        })

    else:
        provisioning_parameters = {
            'ProvisionedProductId': account['ProvisionedProductId'],
            'IgnoreErrors': False
        }

        response = service_catalog.terminate_provisioned_product(**provisioning_parameters)


    logger.info(f"{account['OperationalStatus'].capitalize()} product with the id {account['ProvisionedProductId']}")

    status_message = __check_provisioning_status(account['ProvisionedProductId'])

    if status_message is not None:
        account.update({
            'OperationalStatus': 'FAILED',
            'StatusMessage': status_message
        })

        send_notification(f'{action}_START', 'FAILED', account, status_message)

    else:
        send_notification(f'{action}_START', 'SUCCEEDED', account)


    if action != 'UNENROL':
        update_params = {
            'AwsAccountName': account['AwsAccountName'],
            'AwsAccountId': None if 'AwsAccountId' in account else account['AwsAccountId'],
            'AttributesToUpdate': {
                'ProvisionedProductId': account['ProvisionedProductId'],
                'OperationalStatus': account['OperationalStatus']
            }
        }

        logger.debug(f'Parameters for account update: {update_params}')

        account_register.update_account(**update_params)

    else:
        logger.debug(f"Removing account {account['AwsAccountName']}")

        account_register.delete_account(AwsAccountName = account['AwsAccountName'])


    return