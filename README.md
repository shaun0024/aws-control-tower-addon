# AWS CONTROL TOWER ADD-ON

## Description
This solution was developed as an add-on to overcome some of the limitations when using AWS Control Tower. Some of these limitations include:
- GUI driven in nature, where provisioning, enrollments and updates to managed AWS accounts must be initiated through the Control Tower service page.
- Control Tower is single-threaded and can only perform one provisioning, enrollment or update operation at a time.
- Fixed set of fields in the account form where additional metadata (e.g. business unit that owns the account, contact details of the account, etc.) related to the account cannot be added.
- Executing post provisioning / enrollment processes requires the deployment of CloudFormation StackSet instances using Control Tower Customizations.

With this add-on:
- Provisioning, enrollments, updates and unenrollments can be  executed through the use of a REST API to simplify integration with applications.
- Queues many accounts that are to be provisioned, enrolled, updated or unenrolled to ensure that they are executed one at a time.
- Creates an account registry / database that acts as a source of truth on additional details of the account (e.g. which business unit owns the account, who to contact regarding the account).
- E-mail notifications are sent to provide updates on the progress of the provisioning, enrollment, update or unenrollment process.
- Executes post provisioning / enrollment processes without the need to use CloudFormation StackSet instances.

Aspects of this solution were developed using information and ideas from the following [blog post](https://aws.amazon.com/blogs/mt/how-to-automate-the-creation-of-multiple-accounts-in-aws-control-tower/) from AWS.


## Solution Overview

The diagram below is a high level illustration of the solution design.
![High Level Solution Design](docs/screenshots/solution-design.png)

The following is a description of the actions taken during each step:

1. The API Gateway endpoint is invoked by an application (e.g. ServiceNow).
2. The endpoint on API Gateway triggers the API Responder Lambda.
3. The API Responder Lambda performs the following actions:
    - a. The payload from the API Gateway that contains the details of the AWS account is placed in an Account SQS queue.
    - b. The DynamoDB Account Register is updated with the details and status of the AWS account.
4. An EventBridge Scheduler that runs at an interval triggers the Account Processor Lambda.
5. The Account Processor Lambda performs the following actions:
    - a. An item from the Account SQS queue is retrieved for processing.
    - b. An IAM Service Role is assumed from the Control Tower AWS account to use the Account Factory product in the Service Catalog (created as part of Control Tower) to provision / enrol / update the managed AWS account.
    - c. The DynamoDB Account Register is updated with details of the managed AWS account (e.g. Service Catalog product id, version, etc.)
    - d. A notification is sent to the managed AWS account root address.
6. In the Control Tower account, the Account Factory product in the Service Catalog provisions / enrols / updates the managed AWS account.
7. In the Control Tower account, a Control Tower Lifecycle Event is generated and sent to the EventBridge.
8. In the Control Tower account, a custom EventBridge bus for Account Management filters the events and forwards the event to an SNS topic in the account hosting the Control Tower AddOn.
9. The SNS topic triggers several Lambdas:
    - 9a. The Account Register Lambda performs the following actions:
        - 8a-1. The DynamoDB Account Register is updated with the details of the managed AWS account (e.g. AWS account id, status, etc.)
        - 8a-2. A notification is sent to the managed AWS account root address.
    - 9b. The Account Contact Updater Lambda performs the following actions:
        - 8b-1. The contact details (e.g. primary contact, technical contact, etc.) is obtained from the SSM Parameter Store.
        - 8b-2. An IAM Service Role is assumed from the Control Tower AWS account to update the contact details in the managed AWS account using Organizations.

**IMPORTANT NOTE: The API Gateway provided as part of this code does not include any authentication and, as such, should not be used as is. At the bare minimum, you should modify the template to allow an IAM user to invoke the API Gateway.**


## Frequently Asked Questions

1. **Can I deploy the entire stack into the Control Tower acount?**
While this is technically possible, it is generally recommended that you leave the Control Tower account on its own.

2. **Does this work with Control Tower Customizations?**
Yes, this works independently of Control Tower Customizations

3. **Can I modify the type of details that I want to capture in the Account Register?**
Yes, please refer to xxx.

4. **Why do you use DynamoDB as the Account Register?**
For simplicity. It is perfectly feasible to utilise some other database / storage like RDS or ServiceNow CMDB for example. You can modify the file ```account_register.py``` if you wanted to use something else. I am personally interested in using ServiceNow CMDB but have limited knowledge on it but if you are familiar with it and are interested in extending this further, contact me directly.

5. **Can I include additional actions to be executed after the managed AWS account has been provisioned / enrolled?**
Yes, currently, this sample code performs two post provisioning / enrollment actions:
    - Updates the DynamoDB Account Register
    - Updates the contact details in the managed AWS account

    If you want to perform additional actions, hook up additional Lambdas to the Account Management SNS topic. Do note however that if you wanted to perform actions directly on the managed AWS accounts, you will need to be able to assume a service role in those accounts.


## Deployment Guide
Please refer to [deployment guide](docs/01-deployment.md).