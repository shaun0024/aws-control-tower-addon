# CUSTOMIZATION

## Account and Contact Schema
The Account and Contact schema can be customised by modifying the ```get_schema_attributes``` function in the file ```validator.py```.

Add the new entry / entries into the ```extended_account_attributes``` and / or ```contact_attributes``` dictionary in the following format:

```
    extended_account_attributes = {
        'OwningBusinessUnit': {
            'Mandatory': True,
            'Type': str,
            'Regex': r'^BU(\d){3,6}$'
        },
        'CostCode': {
            'Mandatory': True,
            'Type': str,
            'Regex': r'^COST(\d){3,6}$'
        },
        'Comments': {
            'Mandatory': False,
            'Type': str
        }
    }
```

**Example 1 - Add mandatory attribute to Account schema**

To add a mandatory attribute called ```Department```:

```
    extended_account_attributes = {
        'OwningBusinessUnit': {
            'Mandatory': True,
            'Type': str,
            'Regex': r'^BU(\d){3,6}$'
        },
        ...
        'Department': {
            'Mandatory': True,
            'Type': str
        }
    }
```

**Example 2 - Add optional attribute validated by regular expression to Account schema**

To add an optional attribute called ```ProjectCode``` with a format of ```PROJ1234```:

```
    extended_account_attributes = {
        'OwningBusinessUnit': {
            'Mandatory': True,
            'Type': str,
            'Regex': r'^BU(\d){3,6}$'
        },
        ...
        'ProjectCode': {
            'Mandatory': False,
            'Type': str,
            'Regex': r'^PROJ(\d){4}$'
        }
    }
```

**Example 3 - Add optional attribute validated to Contact schema**

To add an optional attribute called ```Location```:

```
    contact_attributes = {
        'ContactName': {
            'Mandatory': True,
            'Type': str
        },
        ...
        'Location': {
            'Mandatory': False,
            'Type': str
        }
    }
```


## Notification
The contents of the notification e-mail sent can be customised by modifying the ```__get_template``` function in the file ```notification.py```.

Modify the ```Subject``` and ```Body``` of the respective operations (e.g. ```PROVISION_START```, ```ENROL_START```, etc.) ```templates``` dictionary. You can utilise attributes within the Account schema as part of the message.

**Example - Modify the Body of the PROVISION_END operation to include the OwningBusinessUnit**

```
    templates = {
        ...,
        'PROVISION_END': {
            'SUCCEEDED': {
                'Subject': f"AWS account {account['AwsAccountName']} ({account['AwsAccountId']}) is ready",
                'Body': f"AWS account {account['AwsAccountName']} ({account['AwsAccountId']}) belonging to {account['OwningBusinessUnit']} is ready. Please set up MFA."
            },
            'FAILED': {
                'Subject': f"AWS account {account['AwsAccountName']} ({account['AwsAccountId']}) was not successfully provisioned / enrolled",
                'Body': f"AWS account {account['AwsAccountName']} ({account['AwsAccountId']}) was not successfully provisioned / enrolled"
            }
        },
        ...
    }
```
