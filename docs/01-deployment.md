# DEPLOYMENT

## Pre-requisites

AWS Control Tower must have been set up before proceeding further. In addition, this was developed using two separate AWS accounts:
1. One AWS account that hosts Control Tower. This will be referred to as the ```Control Tower Account```
2. Another AWS account that hosts the AddOn. This will be referred to as the ```Control Tower AddOn Account```

This has only been tested with both Control Tower and the AddOn running in the same region.

### From the Control Tower Account

**Get the AWS Organization ID**
1. Access the ```AWS Organizations``` console and take note of the account id.
2. Take note of the ```Organization ID``` on the left pane.
![Get AWS Organization ID](screenshots/get-aws-organizations-id.png)

**Get the AWS Service Catalog Portfolio ID**
1. Access the ```AWS Service Catalog``` console.
2. Navigate to ```Portfolios``` from the left pane.
3. Select the  ```AWS Control Tower Account Factory Portfolio``` product.
4. Take note of the ```Portfolio ID``` under the product details.
![Get AWS Service Catalog Portfolio ID](screenshots/get-aws-service-catalog-portfolio-id.png)

**Get the AWS Service Catalog Product ID**
1. Access the ```AWS Service Catalog``` console.
2. Navigate to ```Product list``` from the left pane.
3. Select the  ```AWS Control Tower Account Factory``` product.
4. Take note of the ```Product ID``` under the product details.
![Get AWS Service Catalog Product ID](screenshots/get-aws-service-catalog-product-id.png)

**Enable AWS Account Management in AWS Organizations**
1. Access the ```AWS Organizations``` console.
2. Navigate to ```Services``` from the left pane.
3. Select ```AWS Account Management``` and enable access.
![Enable Account Management in AWS Organizations](screenshots/enable-organizations-account-management.png)


### From the Control Tower AddOn Account

**Verify domain with Amazon Simple Email Service (SES)**
1. Notifications are sent using SES and the entire domain needs to be verified. Refer to the [following link].(https://docs.aws.amazon.com/ses/latest/dg/creating-identities.html#just-verify-domain-proc)
2. Once verified, take note of the ARN of the verified domain.
![Get ARN of verified SES identity](screenshots/get-amz-ses-identity-arn.png)






## Deploy the CloudFormation Stack in the Control Tower AddOn Account
1. Review the parameters required for deployment in the table below. Some of these would have been obtained in the pre-requisite steps earlier.

| Parameter Name | Description |
|---|---|
| AccountContactsParamBasePath | The base path in which the contact parameters will be created. Exclude '/' in the start and end (e.g. account/contacts). |
| AccountPrimaryContact | JSON string of the primary contact for AWS accounts in the following format: ```{"FullName":"Some Company Co","AddressLine1":"123 Some Road","City":"Melbourne","PostalCode":"3000","StateOrRegion":"Victoria","CompanyName":"Some Company Co","CountryCode":"AU","PhoneNumber":"+61387654321","WebsiteUrl":"https://somedomain.com"}``` |
| AccountBillingContact | JSON string of the billing contact for AWS accounts in the following format: ```{"Name":"Cloud Billing","Title":"Cloud FinOps, Some Company Co","EmailAddress":"billing@somedomain.com","PhoneNumber":"+61387654321"}``` |
| AccountOperationsContact | JSON string of the operations contact for AWS accounts in the following format: ```{"Name":"Cloud Operations","Title":"Cloud Operations, Some Company Co","EmailAddress":"billing@somedomain.com","PhoneNumber":"+61387654321"}``` |
| AccountSecurityContact | JSON string of the security contact for AWS accounts in the following format: ```{"Name":"Cloud Security","Title":"Security, Some Company Co","EmailAddress":"billing@somedomain.com","PhoneNumber":"+61387654321"}``` |
| ControlTowerAccountId | The AWS account id of the Control Tower account.|
| AccountFactoryProductId | The product id for the Account Factory product in the AWS Service Catalog. |
| LoggingLevel | The logging level for the Lambdas. Accepted values are ```info```, ```error```, ```warning```, ```debug```. Default is ```info```. |
| SesIdentityArn | The ARN of the verified identity in Amazon Simple E-mail Service used for sending notifications. |
| NotificationFromAddress | The from address used when sending notifications (e.g. no-reply@domain.com). |

2. Once done, identify the region to be used and click on the link to launch the CloudFormation service page.

| Region | Launch Link |
|---|---|
| US East (N. Virginia) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://us-east-1.console.aws.amazon.com/cloudformation/home?region=us-east-1#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-addon-packaged.yaml&stackName=ControlTowerAddOnAPI) |
| US East (Ohio) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://us-east-2.console.aws.amazon.com/cloudformation/home?region=us-east-2#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-addon-packaged.yaml&stackName=ControlTowerAddOnAPI) |
| US West (Oregon) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://us-west-2.console.aws.amazon.com/cloudformation/home?region=us-west-2#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-addon-packaged.yaml&stackName=ControlTowerAddOnAPI) |
| Canada (Central) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ca-central-1.console.aws.amazon.com/cloudformation/home?region=ca-central-1#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-addon-packaged.yaml&stackName=ControlTowerAddOnAPI) |
| Asia Pacific (Singapore) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-southeast-1.console.aws.amazon.com/cloudformation/home?region=ap-southeast-1#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-addon-packaged.yaml&stackName=ControlTowerAddOnAPI) |
| Asia Pacific (Sydney) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-southeast-2.console.aws.amazon.com/cloudformation/home?region=ap-southeast-2#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-addon-packaged.yaml&stackName=ControlTowerAddOnAPI) |
| Asia Pacific (Mumbai) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-south-1.console.aws.amazon.com/cloudformation/home?region=ap-south-1#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-addon-packaged.yaml&stackName=ControlTowerAddOnAPI) |
| Asia Pacific (Tokyo) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-northeast-1.console.aws.amazon.com/cloudformation/home?region=ap-northeast-1#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-addon-packaged.yaml&stackName=ControlTowerAddOnAPI) |
| Asia Pacific (Seoul) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-northeast-2.console.aws.amazon.com/cloudformation/home?region=ap-northeast-2#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-addon-packaged.yaml&stackName=ControlTowerAddOnAPI) |
| Europe (Frankfurt) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://eu-central-1.console.aws.amazon.com/cloudformation/home?region=eu-central-1#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-addon-packaged.yaml&stackName=ControlTowerAddOnAPI) |
| Europe (Ireland) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://eu-west-1.console.aws.amazon.com/cloudformation/home?region=eu-west-1#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-addon-packaged.yaml&stackName=ControlTowerAddOnAPI) |
| Europe (London) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://eu-west-2.console.aws.amazon.com/cloudformation/home?region=eu-west-2#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-addon-packaged.yaml&stackName=ControlTowerAddOnAPI) |
| Europe (Paris) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://eu-west-3.console.aws.amazon.com/cloudformation/home?region=eu-west-3#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-addon-packaged.yaml&stackName=ControlTowerAddOnAPI) |
| Europe (Stockholm) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://eu-north-1.console.aws.amazon.com/cloudformation/home?region=eu-north-1#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-addon-packaged.yaml&stackName=ControlTowerAddOnAPI) |
| South America (Sao Paulo) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://sa-east-1.console.aws.amazon.com/cloudformation/home?region=sa-east-1#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-addon-packaged.yaml&stackName=ControlTowerAddOnAPI) |





## 3.1 Deploy the CloudFormation Stack in the Control Tower Account
1. Review the parameters required for deployment in the table below. Some of these would have been obtained in the pre-requisite steps earlier.

| Parameter Name | Description |
|---|---|
| OrganizationId | The AWS Organization id used by Control Tower. |
| AccounttFactoryPortfolioId | The portfolio id for the Account Factory portfolio in the AWS Service Catalog. |
| AccountFactoryProductId | The product id for the Account Factory product in the AWS Service Catalog. |
| ControlTowerAddOnAccountId | The AWS account id of the Control Tower AddOn account. |

2. Once done, identify the region to be used and click on the link to launch the CloudFormation service page.

| Region | Launch Link |
|---|---|
| US East (N. Virginia) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://us-east-1.console.aws.amazon.com/cloudformation/home?region=us-east-1#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-cross-account-resources.yaml&stackName=ControlTowerAddOnAPI) |
| US East (Ohio) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://us-east-2.console.aws.amazon.com/cloudformation/home?region=us-east-2#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-cross-account-resources.yaml&stackName=ControlTowerAddOnAPI) |
| US West (Oregon) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://us-west-2.console.aws.amazon.com/cloudformation/home?region=us-west-2#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-cross-account-resources.yaml&stackName=ControlTowerAddOnAPI) |
| Canada (Central) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ca-central-1.console.aws.amazon.com/cloudformation/home?region=ca-central-1#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-cross-account-resources.yaml&stackName=ControlTowerAddOnAPI) |
| Asia Pacific (Singapore) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-southeast-1.console.aws.amazon.com/cloudformation/home?region=ap-southeast-1#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-cross-account-resources.yaml&stackName=ControlTowerAddOnAPI) |
| Asia Pacific (Sydney) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-southeast-2.console.aws.amazon.com/cloudformation/home?region=ap-southeast-2#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-cross-account-resources.yaml&stackName=ControlTowerAddOnAPI) |
| Asia Pacific (Mumbai) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-south-1.console.aws.amazon.com/cloudformation/home?region=ap-south-1#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-cross-account-resources.yaml&stackName=ControlTowerAddOnAPI) |
| Asia Pacific (Tokyo) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-northeast-1.console.aws.amazon.com/cloudformation/home?region=ap-northeast-1#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-cross-account-resources.yaml&stackName=ControlTowerAddOnAPI) |
| Asia Pacific (Seoul) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://ap-northeast-2.console.aws.amazon.com/cloudformation/home?region=ap-northeast-2#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-cross-account-resources.yaml&stackName=ControlTowerAddOnAPI) |
| Europe (Frankfurt) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://eu-central-1.console.aws.amazon.com/cloudformation/home?region=eu-central-1#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-cross-account-resources.yaml&stackName=ControlTowerAddOnAPI) |
| Europe (Ireland) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://eu-west-1.console.aws.amazon.com/cloudformation/home?region=eu-west-1#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-cross-account-resources.yaml&stackName=ControlTowerAddOnAPI) |
| Europe (London) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://eu-west-2.console.aws.amazon.com/cloudformation/home?region=eu-west-2#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-cross-account-resources.yaml&stackName=ControlTowerAddOnAPI) |
| Europe (Paris) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://eu-west-3.console.aws.amazon.com/cloudformation/home?region=eu-west-3#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-cross-account-resources.yaml&stackName=ControlTowerAddOnAPI) |
| Europe (Stockholm) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://eu-north-1.console.aws.amazon.com/cloudformation/home?region=eu-north-1#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-cross-account-resources.yaml&stackName=ControlTowerAddOnAPI) |
| South America (Sao Paulo) | [![Launch Stack](https://cdn.rawgit.com/buildkite/cloudformation-launch-stack-button-svg/master/launch-stack.svg)](https://sa-east-1.console.aws.amazon.com/cloudformation/home?region=sa-east-1#/stacks/create/review?templateURL=https://jlyfish-infra-assets-public.s3.ap-southeast-2.amazonaws.com/cf-templates/41013810/control-tower-cross-account-resources.yaml&stackName=ControlTowerAddOnAPI) |