# USAGE

## Provision a New AWS Account

To provision a new AWS account managed by Control Tower, use the following endpoint:

```POST https://{AwsApiGatewayUrl}/{AwsApiGatewayStage}/aws/account```

### URI Parameters

| Name | Description |
|---|---|
| AwsApiGatewayUrl | The URL of the AWS API Gateway. |
| AwsApiGatewayStage | The stage of the AWS API Gateway. |
| AwsAccountId | The AWS account id of the account to be enrolled.

### Request Body

| Attribute | Type | Required? | Description |
|---|---|---|---|
| ```AwsAccountName``` | String | Yes |The name of the AWS account. This is not the AWS account alias. |
| ```AwsAccountRootAddress``` | String | Yes | The root address of the AWS account. |
| ```AwsOrganisationalUnitId``` | String | Yes | The id of the organisational unit in AWS Organization (e.g. ou-2tfy-x233cfpd). |
| ```AwsAccountSSOAddress``` | String | Yes | The address used for the first user for AWS Identity Center. |
| ```AwsAccountSSOFirstName``` | String | Yes | The first name of the first user for AWS Identity Center. |
| ```AwsAccountSSOLastName``` | String | Yes | The last name of the first user for AWS Identity Center. |
| ```OwningBusinessUnit``` | String | Yes | The id of the business unit who owns the AWS account. |
| ```CostCode``` | String | Yes | The cost code associated with the AWS account. |
| ```AccountContacts``` | Array of Contact Objects | Yes | An array containing contact details for owners of the account. See contact shema below. At least 1 contact is required. |
| ```TechnicalContacts``` | Array of Contact Objects | Yes | An array containing technical contact details of the account. See contact shema below. At least 1 contact is required. |
| ```OperationalContacts``` | Array of Contact Objects | Yes | An array containing operational contact details of the account. See contact shema below.At least 1 contact is required. |
| ```Comments``` | String | No | Free text to store any additional information about the account. |

**NOTE: To customise this, please refer to xxx**

**Example**
```
{
  "AwsAccountName":"some-company-aws-account-123",
  "AwsAccountRootAddress":"aws-account-123@somewhere.com",
  "AwsOrganisationalUnitId":"ou-1sex-w122beoc",
  "AwsAccountSSOAddress":"john.doe@somewhere.com",
  "AwsAccountSSOFirstName":"John",
  "AwsAccountSSOLastName":"Doe",
  "OwningBusinessUnit":"BU12345",
  "CostCode":"COST1234",
  "AccountContacts":[
    {
      "ContactName":"John Doe",
      "ContactEmail":"john.doe@somewhere.com"
    },
    {
      "ContactName":"Jane Doe",
      "ContactEmail":"jane.doe@somewhere.com"
    }
  ],
  "TechnicalContacts":[
    {
      "ContactName":"John Doe",
      "ContactEmail":"john.doe@somewhere.com"
    },
    {
      "ContactName":"Jane Doe",
      "ContactEmail":"jane.doe@somewhere.com"
    }
  ],
  "OperationalContacts":[
    {
      "ContactName":"Jane Doe",
      "ContactEmail":"jane.doe@somewhere.com"
    }
  ],
  "Comments":"This account is used for Application A"
}
```


## Enrol Existing AWS Account Into Control Tower

To enrol an existing AWS account into Control Tower, use the following endpoint:

```POST https://{AwsApiGatewayUrl}/{AwsApiGatewayStage}/aws/account/{AwsAccountId}```

**NOTE: The AWS account must already be part of the AWS Organization**

### URI Parameters

| Name | Description |
|---|---|
| AwsApiGatewayUrl | The URL of the AWS API Gateway. |
| AwsApiGatewayStage | The stage of the AWS API Gateway. |
| AwsAccountId | The AWS account id of the account to be enrolled.

### Request Body

| Attribute | Type | Required? | Description |
|---|---|---|---|
| ```AwsOrganisationalUnitId``` | String | Yes | The id of the organisational unit in AWS Organization (e.g. ou-2tfy-x233cfpd). |
| ```OwningBusinessUnit``` | String | Yes | The id of the business unit who owns the AWS account. |
| ```CostCode``` | String | Yes | The cost code associated with the AWS account. |
| ```AccountContacts``` | Array of Contact Objects | Yes | An array containing contact details for owners of the account. See contact shema below. At least 1 contact is required. |
| ```TechnicalContacts``` | Array of Contact Objects | Yes | An array containing technical contact details of the account. See contact shema below. At least 1 contact is required. |
| ```OperationalContacts``` | Array of Contact Objects | Yes | An array containing operational contact details of the account. See contact shema below.At least 1 contact is required. |
| ```Comments``` | String | No | Free text to store any additional information about the account. |

**NOTE: To customise this, please refer to xxx**

**Example**
```
{
  "AwsOrganisationalUnitId":"ou-1sex-w122beoc",
  "OwningBusinessUnit":"BU12345",
  "CostCode":"COST1234",
  "AccountContacts":[
    {
      "ContactName":"John Doe",
      "ContactEmail":"john.doe@somewhere.com"
    },
    {
      "ContactName":"Jane Doe",
      "ContactEmail":"jane.doe@somewhere.com"
    }
  ],
  "TechnicalContacts":[
    {
      "ContactName":"John Doe",
      "ContactEmail":"john.doe@somewhere.com"
    },
    {
      "ContactName":"Jane Doe",
      "ContactEmail":"jane.doe@somewhere.com"
    }
  ],
  "OperationalContacts":[
    {
      "ContactName":"Jane Doe",
      "ContactEmail":"jane.doe@somewhere.com"
    }
  ],
  "Comments":"This account is used for Application A"
}
```


## Update an AWS Account Managed By Control Tower

To enrol an existing AWS account into Control Tower, use the following endpoint:

```PATCH https://{AwsApiGatewayUrl}/{AwsApiGatewayStage}/aws/account/{AwsAccountId}```

**NOTE: The AWS account must have been enrolled using this solution**

### URI Parameters

| Name | Description |
|---|---|
| AwsApiGatewayUrl | The URL of the AWS API Gateway. |
| AwsApiGatewayStage | The stage of the AWS API Gateway. |
| AwsAccountId | The AWS account id of the account to be enrolled.

### Request Body

| Attribute | Type | Required? | Description |
|---|---|---|---|
| ```AwsOrganisationalUnitId``` | String | No | The id of the organisational unit in AWS Organization (e.g. ou-2tfy-x233cfpd). |
| ```AwsAccountSSOAddress``` | String | No | The address used for the first user for AWS Identity Center. |
| ```AwsAccountSSOFirstName``` | String | No | The first name of the first user for AWS Identity Center. |
| ```AwsAccountSSOLastName``` | String | No | The last name of the first user for AWS Identity Center. |
| ```OwningBusinessUnit``` | String | No | The id of the business unit who owns the AWS account. |
| ```CostCode``` | String | No | The cost code associated with the AWS account. |
| ```AccountContacts``` | Array of Contact Objects | No | An array containing contact details for owners of the account. See contact shema below. If used, this will overwrite the entire array used originally. |
| ```TechnicalContacts``` | Array of Contact Objects | No | An array containing technical contact details of the account. See contact shema below. At least 1 contact is required. If used, this will overwrite the entire array used originally. |
| ```OperationalContacts``` | Array of Contact Objects | No | An array containing operational contact details of the account. See contact shema below.At least 1 contact is required. If used, this will overwrite the entire array used originally. |
| ```Comments``` | String | No | Free text to store any additional information about the account. |

**NOTES:**
- Updates to AwsOrganisationalUnitId, AwsAccountSSOAddress, AwsAccountSSOFirstName, AwsAccountSSOLastName will queue the account for processing and as such, changes may not be noticed immediately. For other attributes, the account register will be updated immediately.
- To customise this, please refer to xxx

**Example 1 - Update the AWS Organizational OU that the account will be placed under**
```
{
    "AwsOrganisationalUnitId":"ou-2tfy-x233cfpd"
}
```

**Example 2 - Update the Cost Code and Comments**
```
{
    "CostCode":"COST1234",
    "Comments":"This account is used for Application A"
}
```


## Get Details of an AWS Account Managed By Control Tower

To enrol an existing AWS account into Control Tower, use the following endpoint:

```DELETE https://{AwsApiGatewayUrl}/{AwsApiGatewayStage}/aws/account/{AwsAccountId}```

**NOTE: The AWS account must have been enrolled using this solution**

### URI Parameters

| Name | Description |
|---|---|
| AwsApiGatewayUrl | The URL of the AWS API Gateway. |
| AwsApiGatewayStage | The stage of the AWS API Gateway. |
| AwsAccountId | The AWS account id of the account to be enrolled.



## Unenrol an AWS Account Managed By Control Tower

To enrol an existing AWS account into Control Tower, use the following endpoint:

```DELETE https://{AwsApiGatewayUrl}/{AwsApiGatewayStage}/aws/account/{AwsAccountId}```

**NOTE: The AWS account must have been enrolled using this solution**

### URI Parameters

| Name | Description |
|---|---|
| AwsApiGatewayUrl | The URL of the AWS API Gateway. |
| AwsApiGatewayStage | The stage of the AWS API Gateway. |
| AwsAccountId | The AWS account id of the account to be enrolled.



# CONTACT SCHEMA

| Attribute | Type | Required? | Description |
|---|---|---|---|
| ```ContactName``` | String | Yes | The name of the contact. Can be customized (see below). |
| ```ContactTitle``` | String | No | The title of the contact. Can be customized (see below). |
| ```ContactEmail``` | String | Yes | The e-mail of the contact. Can be customized (see below). |
| ```ContactPhone``` | String | No | The phone number of the contact. Can be customized (see below). |